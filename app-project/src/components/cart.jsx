const Cart = ({qty, decrementProduct, incrementProduct, activeImage}) => {
    return <div className="col-span-2">
    <div className="p-2 w-full border rounded-lg mb-4 ">
      <a
        href="https://poolapack.co.id/packer/Mutiara-Abadi-Textile?packer_id=ufsl18z9"
        className="flex items-start gap-1.5"
      >
        <img
          src="https://poolapack.co.id/_nuxt/assets/img/icons/img_default.png"
          className="rounded-full h-11 w-11"
          alt=""
        />
        <div className="flex-col justify-center items-center">
          <span className="block truncate mb-0.5 font-bold text-sm">
            Mutiara Abadi Textile
          </span>
          <div className="w-24 bg-amber-300 rounded-lg px-2 py0.5">
            <label className="block text-center text-[10px] font-medium text-packer-500 cursor-pointer">
              Packer Reguler
            </label>
          </div>
        </div>
      </a>
    </div>
    <div className="w-full rounded-lg p-2 border">
      <h5 className="font-bold text-base text-left">
        Atur Jumlah dan Catatan
      </h5>
      <div className="my-4 p-2 border border-amber-300 rounded-md grid grid-cols-12">
        <div className="col-span-6 border-gray-300 hover:border-pool hover:text-pool p-2 items-center rounded-lg flex">
          <img
            src={activeImage}
            alt=""
            className="rounded-lg h-9 w-9 mr-3"
          />
          <div className="flex text-xs lg:text-sm font-semid=bold">
            <h6>Toyobo Fodu</h6>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12 gap-2 pt-2">
        <div className="col-span-6 flex flex-col">
          <h5 className="text-[0.9rem] font-semibold">Stock Barang:</h5>
          <h6 className="text-lg text-packer-500 font-bold">
            4,920 Yard
          </h6>
        </div>
        <div className="relative col-span-6 items-center flex">
          <div
            className="absolute inset-y-0 left-0 flex items-center pl-3 cursor-pointer"
            onClick={() => decrementProduct()}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-4 h-4 hover:text-pool"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M18 12H6"
              ></path>
            </svg>
          </div>
          <input
            type="number"
            id="qty"
            value={qty}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full pr-10 pl-10 p-2.5 text-center"
          />
          <div
            class="absolute inset-y-0 right-0 flex items-center pr-3 cursor-pointer"
            onClick={() => incrementProduct()}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-4 h-4 hover:text-pool"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M12 6v12m6-6H6"
              ></path>
            </svg>
          </div>
        </div>
      </div>
      <div className="flex justify-between py-2 ">
        <h5 className="flex items-center text-[0.9rem] font-semibold">
          Subtotal :
        </h5>
        <h6 className="text-lg text-amber-500 font-bold">
          Rp. {qty > 3000 ? qty * 19900 : qty * 20000}
        </h6>
      </div>
      <div className="w-full space-y-2 my-2">
        <button
          type="button"
          class="text-white bg-amber-400 hover:bg-amber-500 focus:outline-none focus:ring-4 focus:ring-amber-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 dark:focus:ring-amber-900 w-full font-bold"
        >
          Keranjang
        </button>
        <button
          type="button"
          class="text-white bg-amber-400 hover:bg-amber-500 focus:outline-none focus:ring-4 focus:ring-amber-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 dark:focus:ring-amber-900 w-full font-bold"
        >
          Request Sample
        </button>
      </div>
      <div className="grid grid-cols-12 gap-2">
        <a
          href=""
          className="col-span-4 rounded-md py-1.5 px-2 border border-amber-600 hover:border-amber-400 flex items-center justify-center "
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-5 h-5"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M8.625 9.75a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H8.25m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H12m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0h-.375m-13.5 3.01c0 1.6 1.123 2.994 2.707 3.227 1.087.16 2.185.283 3.293.369V21l4.184-4.183a1.14 1.14 0 01.778-.332 48.294 48.294 0 005.83-.498c1.585-.233 2.708-1.626 2.708-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"
            ></path>
          </svg>
        </a>
        <a
          href=""
          className="col-span-4 rounded-md py-1.5 px-2 border border-amber-600 hover:border-amber-400 flex items-center justify-center "
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-5 h-5"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M8.625 9.75a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H8.25m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H12m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0h-.375m-13.5 3.01c0 1.6 1.123 2.994 2.707 3.227 1.087.16 2.185.283 3.293.369V21l4.184-4.183a1.14 1.14 0 01.778-.332 48.294 48.294 0 005.83-.498c1.585-.233 2.708-1.626 2.708-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"
            ></path>
          </svg>
        </a>
        <a
          href=""
          className="col-span-4 rounded-md py-1.5 px-2 border border-amber-600 hover:border-amber-400 flex items-center justify-center "
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-4 h-4"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M7.217 10.907a2.25 2.25 0 100 2.186m0-2.186c.18.324.283.696.283 1.093s-.103.77-.283 1.093m0-2.186l9.566-5.314m-9.566 7.5l9.566 5.314m0 0a2.25 2.25 0 103.935 2.186 2.25 2.25 0 00-3.935-2.186zm0-12.814a2.25 2.25 0 103.933-2.185 2.25 2.25 0 00-3.933 2.185z"
            ></path>
          </svg>
        </a>
      </div>
    </div>
  </div>
}

export default Cart
const DetailProduct = ({activeImage}) => {
    return <div className="col-span-3 ">
    <div className="mx-auto">
      <div className="flex justify-between items-center w-full bg-yellow-400 p-2 mb-4 rounded-lg">
        <h3 className="text-base font-semibold text-white mb-1">
          Postingan berakhir dalam :
        </h3>
        <div className="flex space-x-2 mt-1">
          <div>
            <div className="w-10 py-1 text-center rounded-t-md rounded-br-md bg-gray-50 text-sm font-bold shadow-xl">
              12
            </div>
            <div className="w-8 pb0.5 text-center rounded-b-md bg-[#4B1312] text-[#F9BE00] font-xs shadow-xl">
              hari
            </div>
          </div>
          <div>
            <div className="w-10 py-1 text-center rounded-t-md rounded-br-md bg-gray-50 text-sm font-bold shadow-xl">
              12
            </div>
            <div className="w-8 pb0.5 text-center rounded-b-md bg-[#4B1312] text-[#F9BE00] font-xs shadow-xl">
              jam
            </div>
          </div>
          <div>
            <div className="w-10 py-1 text-center rounded-t-md rounded-br-md bg-gray-50 text-sm font-bold shadow-xl">
              12
            </div>
            <div className="w-8 pb0.5 text-center rounded-b-md bg-[#4B1312] text-[#F9BE00] font-xs shadow-xl">
              men
            </div>
          </div>
          <div>
            <div className="w-10 py-1 text-center rounded-t-md rounded-br-md bg-gray-50 text-sm font-bold shadow-xl">
              12
            </div>
            <div className="w-8 pb0.5 text-center rounded-b-md bg-[#4B1312] text-[#F9BE00] font-xs shadow-xl">
              det
            </div>
          </div>
        </div>
      </div>
    </div>
    <div>
      <h1 className="font-bold text-[1.3rem] leading-[24px] text-left">
        Toyobo Fodu - Toyobo Fodu
      </h1>
    </div>
    <div className="mt-2">
      <h6 className="text-[0.9rem] font-medium text-left">
        Terjual
        <span className="text-gray-500">80 Yard</span>
      </h6>
    </div>
    <div className="my-2 text-left">
      <div className="grid grid-cols-2">
        <span className="text-amber-500 font-bold">RP.20.000</span>
        <span>RP.20.000</span>
      </div>
    </div>
    <div className="flex relative my-2 pr-3">
      <div className="flex h-[30px] relative">
        <div
          className="h-[38px] bg-gradient-to-r from-[#9E060E] via-[#B5292C] to-[#FF4E3C]  flex justify-between items-center -top-1 left-0 absolute z-30"
          style={{ width: "1.3333%" }}
        >
          <div className="triangle-right-tier left-0"></div>
        </div>
      </div>
      <div className="z-20 flex h-full w-full">
        <div className="bg-yellow-400 w-full justify-between items-center text-center">
          <p className="font-medium text-gray-900 w-full text-lg ">
            RP. 20.000
          </p>
          {/* <div className="triangle-right"></div> */}
        </div>
        <div className="bg-yellow-500 w-full justify-between items-center text-center">
          <p className="font-medium text-gray-900 w-full text-lg">
            RP. 20.000
          </p>
          {/* <div className="triangle-right"></div> */}
        </div>
      </div>
    </div>
    <div className="my-2 text-left">
      <div className="grid grid-cols-2">
        <span className="text-packer-500 font-bold">
          1 &minus; 3000 Yard
        </span>
        <span>&gt; 3000 Yard</span>
      </div>
      <span>Minimal Order Quantity 100 yeard</span>
    </div>
    <div>
      <hr className="bg-gray-100 my-4" />
      <div className="mb-3">
        <h5 className="font-bold text-left">Pilih Varian :</h5>
      </div>
      <div className="grid grid-cols-12 gap-3 mb-3">
        <div className="col-span-6 border border-gray-300 hover:border-pool hover:text-pool p-2 items-center rounded-lg flex">
          <img
            src={activeImage}
            alt=""
            className="rounded-md h-6 w-6 mr-2"
          />
          <div className="flex text-xs lg:text-sm font-semid=bold">
            <h6>Toyobo Fodu</h6>
          </div>
        </div>
      </div>
      <div className="space-y-2">
        <h5 className="font-bold text-left">Detail Produk :</h5>
        <div>
          <table className="text-[0.9rem]">
            <tr>
              <th className="flex text-gray-500 font-normal justify-start w-20">
                Lebar
              </th>
              <th className="w-5 text-gray-900 font-normal">:</th>
              <th className="font-normal"></th>
            </tr>
            <tr>
              <th className="flex text-gray-500 font-normal justify-start w-20">
                Gramasi
              </th>
              <th className="w-5 text-gray-900 font-normal">:</th>
              <th className="font-normal"></th>
            </tr>
            <tr>
              <th className="flex text-gray-500 font-normal justify-start w-20">
                Technique
              </th>
              <th className="w-5 text-gray-900 font-normal">:</th>
              <th className="font-normal"></th>
            </tr>
            <tr>
              <th className="flex text-gray-500 font-normal justify-start w-20">
                Material
              </th>
              <th className="w-5 text-gray-900 font-normal">:</th>
              <th className="font-normal"></th>
            </tr>
          </table>
        </div>
        <div className="text-left">
          <span class="text-[0.9rem] whitespace-pre-line">
            Toyobo Fodu <br />
            Lebar : 150cm <br />
            Gramasi : 145gsm <br />
            Semua yang tertera di katalog ready ya!
          </span>
        </div>
      </div>
    </div>
  </div>
}

export default DetailProduct
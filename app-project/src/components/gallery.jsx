const Gallery = (data) => {
    return <div className="col-span-2 overflow-auto">
    <div className="flex flex-col">
      <img
        src={data.activeImage}
        alt=""
        className="rounded-lg aspect-square"
        value
      />
      <div className="grid grid-cols-5 gap-2 mt-6">
        {data.images.map((image) => {
          return (
            <img
              src={image}
              alt=""
              className="rounded-lg aspect-square hover:outline hover:outline-700"
              onMouseEnter={() => data.changeImage(image)}
            />
          );
        })}
      </div>
    </div>
  </div>
}

export default Gallery;
import { useState } from "react";

import "./App.css";
import "./index.css";
import Gallery from "./components/gallery";
import DetailProduct from "./components/detailProduct";
import Cart from "./components/cart";

function App() {
  const [images, setImages] = useState([
    "https://dligqml0naixs.cloudfront.net/uploads/dtrmpv5p3a1266lmd7u7q4n91_image.jpeg",
    "https://dligqml0naixs.cloudfront.net/uploads/688864t5lkronofm3f4z1v85c_image.jpeg",
  ]);

  const [minQty, setMinQty] = useState(100);
  const [qty, setQty] = useState(100);

  const [activeImage, setActiveImage] = useState(
    "https://dligqml0naixs.cloudfront.net/uploads/dtrmpv5p3a1266lmd7u7q4n91_image.jpeg"
  );

  function decrementProduct() {
    if (qty === minQty || qty <= minQty) {
      setQty(minQty);
      return;
    }
    setQty(qty - 1);
  }

  function changeImage(item) {
    setActiveImage(item)
  }

  function incrementProduct() {
    setQty(qty + 1);
  }

  return (
    <div className="container text-center mx-auto mt-6">
      <div class="grid grid-cols-7 gap-12">
        <Gallery activeImage={activeImage} images={images} changeImage={changeImage}/>
        <DetailProduct activeImage={activeImage}/>
        <Cart qty={qty} decrementProduct={decrementProduct} incrementProduct={incrementProduct} activeImage={activeImage}/>
      </div>
      <div>
        <hr className="bg-gray-100 my-4" />
      </div>
      <div className="w-full flex flex-col">
        <div className="mb-1">
          <h2 className="text-[0.9rem] text-lg text-packer font-semibold text-left">
            Produk Lain Dari Mutiara Abadi Textile
          </h2>
        </div>
      </div>
    </div>
  );
}

export default App;
